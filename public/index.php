<?php require __DIR__ . '/../helper.php'; ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex, nofollow">
    <link rel="icon shortcut" href="favicon.png" />
    <title>My Little Friends</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="/css/lightbox.css"/>
    <link rel="stylesheet" href="/css/screen.css"/>
    <style>
        html, body {

            background-color: #FFD041;
        }

        body {
            margin: 0;
            font-family: "Helvetica Neue Light", "HelveticaNeue-Light", "Helvetica Neue", Calibri, Helvetica, Arial;
        }

        header {
            padding: 0;
            text-align: inherit;
        }

        .menu {
            /*background-color: #B9006E;*/
            background-color: #D14D28;
            height: 67px;
            border-bottom: 2px solid #BD3914;
        }

        .brand {
            font-size: 26px;
            color: #FFFFFF;
            margin-right: 20px;
            outline: none;
            display: inline-block;
            float: left;
            line-height: 67px;
        }

        .brand:hover {
            text-decoration: none;
        }

        .menu-item {
            display: inline-block;
            padding: 20px 15px;
            background-color: #60465F;
            color: #FFFFFF;
            text-align: center;
            cursor: pointer;
        }

        #content {
            margin-top: 28px;
        }

        .container {
            max-width: 1200px;
            width: 100%;
            display: block;
            margin: 0 auto;
        }

        .item {
            width: calc(25% - 23px);
            height: auto;
            padding-bottom: 22px;
        }

        .item > a {
            display: block;

            outline: none;
        }

        img {
            width: 100%;
            height: auto;
            cursor: pointer;

            box-shadow: 2px 2px 20px #000000;
        }

        @media (max-width: 1200px) {
            .container {
                padding-left: 20px;
                padding-right: 20px;
            }
        }

        @media (min-width: 855px) and (max-width: 1200px) {
            .item {
                width: calc(33% - 17px);
            }
        }

        @media (min-width: 655px) and (max-width: 854px) {
            .item {
                width: calc(33% - 18px);
            }
        }

        @media (max-width: 654px) {
            .item {
                width: calc(50% - 15px);
            }
        }
    </style>
</head>
<body>
    <header>
        <nav class="menu" role="navigation">
            <div class="container" role="menu">
                <a href="/" class="brand">My Little Friends</a>
            </div>
        </nav>
    </header>
    <div id="content" class="container">
        <?php $images = images(); ?>
        <div id="masonryContainer">
            <?php foreach($images as $index => $image) : ?>
            <div class="item">
                <a href="<?php echo $image['url']; ?>" data-lightbox="image_<?php echo $index; ?>">
                    <img src="<?php echo $image['url']; ?>" />
                </a>
            </div>
            <?php endforeach; ?>
        </div>
    </div>


    <script src="/js/jquery-1.11.0.min.js"></script>
    <script src="/js/lightbox.min.js"></script>
    <script src="/js/masonry.pkgd.min.js"></script>
    <script src="/js/imagesloaded.pkgd.min.js"></script>
    <script>
        var msnry = new Masonry(document.querySelector("#masonryContainer"), {
            gutter: 30,
            itemSelector: '.item'
        });

        imagesLoaded(document.querySelector('#masonryContainer'), function() {
            msnry.layout();
        });


    </script>
</body>
</html>