<?php

require __DIR__ . '/vendor/autoload.php';
use Aws\S3\S3Client;

function images() {
    $bucket = 'my-little-friends';

    $client = S3Client::factory([
        'key' => 'AKIAJA4M24KHNOVXFWSQ',
        'secret' => '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'
    ]);

    $images = $client->getIterator('ListObjects', [
        'Bucket' => $bucket,
    ]);

    $urls = [];

    foreach($images as $image) {
        $urls[] = [
            'url' => $client->getObjectUrl($bucket, $image['Key'])
        ];
    }

    $urls = array_reverse($urls);

    return $urls;
}

function hex2str($hex) {
    $hexCharacters = [];
    preg_match('~&#[0-9]{1,};~', $hex, $hexCharacters);
    foreach($hexCharacters as $character) {
        $hex = str_replace($character, utf8_encode(chr(preg_replace('~[^0-9]~', '', $character))), $hex);
    }
    return $hex;
}

function format_meta($metadata) {
    $formatted = [];

    foreach($metadata as $key => $data) {
        $formattedData = hex2str($data);
        $formattedKey = ucfirst($key);
        $formatted[] = "<strong>$formattedKey:</strong> $formattedData";
    }

    return implode(' | ', $formatted);
}